const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('book', {
    nam: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: "NO_NAME",
      unique: "book_nam_key"
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    publishing_year: {
      type: DataTypes.DATE,
      allowNull: true
    },
    number: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pages_amount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    id_category: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'category',
        key: 'id_category'
      }
    },
    annotation: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    series: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    in_stock: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize,
    tableName: 'book',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "book_nam_key",
        unique: true,
        fields: [
          { name: "nam" },
        ]
      },
      {
        name: "book_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
