const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('role_', {
    id_role: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: "role_constraint1"
    }
  }, {
    sequelize,
    tableName: 'role_',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "role__pkey",
        unique: true,
        fields: [
          { name: "id_role" },
        ]
      },
      {
        name: "role_constraint1",
        unique: true,
        fields: [
          { name: "name" },
        ]
      },
    ]
  });
};
