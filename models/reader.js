const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reader', {
    id_reader: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    surname: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    patronymic: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ticket_number: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    document_number: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    id_address: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'address',
        key: 'id_address'
      }
    },
    date_of_birth: {
      type: DataTypes.DATE,
      allowNull: true
    },
    phone_number: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'reader',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "reader_pkey",
        unique: true,
        fields: [
          { name: "id_reader" },
        ]
      },
    ]
  });
};
