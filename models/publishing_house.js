const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('publishing_house', {
    id_pubhouse: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    id_address: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'address',
        key: 'id_address'
      }
    }
  }, {
    sequelize,
    tableName: 'publishing_house',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "publishing_house_pkey",
        unique: true,
        fields: [
          { name: "id_pubhouse" },
        ]
      },
    ]
  });
};
