const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('action_with_books', {
    id_awb: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_action: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'action',
        key: 'id_action'
      }
    },
    id_reader: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'reader',
        key: 'id_reader'
      }
    },
    nam: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: "NO_NAME",
      references: {
        model: 'book',
        key: 'nam'
      }
    },
    date_form: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_to: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'action_with_books',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "action_with_books_pkey",
        unique: true,
        fields: [
          { name: "id_awb" },
        ]
      },
    ]
  });
};
