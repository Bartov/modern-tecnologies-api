const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('action', {
    id_action: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "NO_NAME"
    }
  }, {
    sequelize,
    tableName: 'action',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "action_pkey",
        unique: true,
        fields: [
          { name: "id_action" },
        ]
      },
    ]
  });
};
