const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pubhouse_book', {
    id_pubhouse: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'publishing_house',
        key: 'id_pubhouse'
      }
    },
    nam: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: "NO_NAME",
      primaryKey: true,
      references: {
        model: 'book',
        key: 'nam'
      }
    }
  }, {
    sequelize,
    tableName: 'pubhouse_book',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "pubhouse_book_pkey",
        unique: true,
        fields: [
          { name: "nam" },
        ]
      },
    ]
  });
};
