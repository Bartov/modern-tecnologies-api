const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('author_book', {
    id_author: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'author',
        key: 'id_author'
      }
    },
    nam: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: "NO_NAME",
      references: {
        model: 'book',
        key: 'nam'
      }
    }
  }, {
    sequelize,
    tableName: 'author_book',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "author_book_pkey",
        unique: true,
        fields: [
          { name: "id_author" },
        ]
      },
    ]
  });
};
