var DataTypes = require("sequelize").DataTypes;
var _action = require("./action");
var _action_with_books = require("./action_with_books");
var _address = require("./address");
var _author = require("./author");
var _author_book = require("./author_book");
var _book = require("./book");
var _category = require("./category");
var _pubhouse_book = require("./pubhouse_book");
var _publishing_house = require("./publishing_house");
var _reader = require("./reader");
var _role_ = require("./role_");
var _user_ = require("./user_");
var _user_role = require("./user_role");

function initModels(sequelize) {
  var action = _action(sequelize, DataTypes);
  var action_with_books = _action_with_books(sequelize, DataTypes);
  var address = _address(sequelize, DataTypes);
  var author = _author(sequelize, DataTypes);
  var author_book = _author_book(sequelize, DataTypes);
  var book = _book(sequelize, DataTypes);
  var category = _category(sequelize, DataTypes);
  var pubhouse_book = _pubhouse_book(sequelize, DataTypes);
  var publishing_house = _publishing_house(sequelize, DataTypes);
  var reader = _reader(sequelize, DataTypes);
  var role_ = _role_(sequelize, DataTypes);
  var user_ = _user_(sequelize, DataTypes);
  var user_role = _user_role(sequelize, DataTypes);

  action_with_books.belongsTo(action, { as: "id_action_action", foreignKey: "id_action"});
  action.hasMany(action_with_books, { as: "action_with_books", foreignKey: "id_action"});
  publishing_house.belongsTo(address, { as: "id_address_address", foreignKey: "id_address"});
  address.hasMany(publishing_house, { as: "publishing_houses", foreignKey: "id_address"});
  reader.belongsTo(address, { as: "id_address_address", foreignKey: "id_address"});
  address.hasMany(reader, { as: "readers", foreignKey: "id_address"});
  author_book.belongsTo(author, { as: "id_author_author", foreignKey: "id_author"});
  author.hasOne(author_book, { as: "author_book", foreignKey: "id_author"});
  action_with_books.belongsTo(book, { as: "nam_book", foreignKey: "nam"});
  book.hasMany(action_with_books, { as: "action_with_books", foreignKey: "nam"});
  author_book.belongsTo(book, { as: "nam_book", foreignKey: "nam"});
  book.hasMany(author_book, { as: "author_books", foreignKey: "nam"});
  pubhouse_book.belongsTo(book, { as: "nam_book", foreignKey: "nam"});
  book.hasOne(pubhouse_book, { as: "pubhouse_book", foreignKey: "nam"});
  book.belongsTo(category, { as: "id_category_category", foreignKey: "id_category"});
  category.hasMany(book, { as: "books", foreignKey: "id_category"});
  pubhouse_book.belongsTo(publishing_house, { as: "id_pubhouse_publishing_house", foreignKey: "id_pubhouse"});
  publishing_house.hasMany(pubhouse_book, { as: "pubhouse_books", foreignKey: "id_pubhouse"});
  action_with_books.belongsTo(reader, { as: "id_reader_reader", foreignKey: "id_reader"});
  reader.hasMany(action_with_books, { as: "action_with_books", foreignKey: "id_reader"});
  user_role.belongsTo(role_, { as: "id_role_role_", foreignKey: "id_role"});
  role_.hasMany(user_role, { as: "user_roles", foreignKey: "id_role"});
  user_role.belongsTo(user_, { as: "id_user_user_", foreignKey: "id_user"});
  user_.hasOne(user_role, { as: "user_role", foreignKey: "id_user"});

  return {
    action,
    action_with_books,
    address,
    author,
    author_book,
    book,
    category,
    pubhouse_book,
    publishing_house,
    reader,
    role_,
    user_,
    user_role,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
