require('dotenv').config()
const express = require('express')
const sequelize = require('./db')
const initModels = require("./models/init-models").initModels;
const models = initModels(sequelize);
const cors = require('cors')
const req = require('express/lib/request')
const router = require('./routes/index')
const errorHendler = require('./middleware/ErrorHandlingMiddleware')

const PORT = process.env.PORT
const app = express()
app.use(cors())
app.use(express.json())
app.use('/api', router)
app.use(errorHendler)


const start = async () => {
    try {
        await sequelize.authenticate()
        await sequelize.sync()
        app.listen(PORT, () => console.log(`server started on post ${PORT}`))
    } catch (e) {
        console.log(e)
    }
}

start()//

