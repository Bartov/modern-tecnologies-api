const jwt = require('jsonwebtoken')

module.exports = {
checkOneRole: function(role){ 
return function(req, res, next) {
    if (req.method === "OPTIONS") {
        next()
    }
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) {
            return res.status(401).json({message: "Не авторизован"})
        } 
        const decoded = jwt.verify(token, process.env.JWT)
        if (decoded.role !== role) {
            return res.status(403).json({message: "Нет доступа"})
        }
        req.user = decoded
        next()
    }
    catch (e) {
        res.status(401).json({message: "Не авторизован"})
    }
}}, 
checkTwoRole: function(role1,role2){ 
return function(req, res, next) {
    if (req.method === "OPTIONS") {
        next()
    }
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) {
            return res.status(401).json({message: "Не авторизован"})
        } 
        console.log(jwt.verify(token, process.env.JWT));
        const decoded = jwt.verify(token, process.env.JWT)
        if (decoded.role !== role1 && decoded.role !== role2) {
            return res.status(403).json({message: "Нет доступа"})
        }
        req.user = decoded
        next()
    }
    catch (e) {
        res.status(401).json({message: "Не авторизован"})
    }
}}}