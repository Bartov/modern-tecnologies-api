const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const book = models.book

const ApiError = require('../error/ApiError')

class CategoryController {
    async create(req, res, next) {
        try {
            const {nam, name, publishing_year, number, pages_amount,id_category,annotation,series,in_stock} = req.body
            const _book = await book.create({nam, name, publishing_year, number, pages_amount,id_category,annotation,series,in_stock})
            return res.json(_book)
        }
        catch(e){
            next(ApiError.badRequest(e.message))
        }
        
    }
    async getAll(req, res) {
        const {id_category} = req.query
        let books;
        if(!id_category||id_category=='undefined'){
            books = await book.findAll()
        }
        if (id_category&&id_category!='undefined') {
            books = await book.findAll({where:{id_category}})
        }
        
        return res.json(books)
    }
    async get(req, res) {
        try {
            const {nam} = req.params
            const _book = await book.findOne(
                {where : {nam}}
            )
            return res.json(_book)
        }
        catch(e){
            return res.json(e.message)
        }
    }
}

module.exports = new CategoryController()