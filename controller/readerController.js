const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const reader = models.reader

const ApiError = require('../error/ApiError')

class ReaderController {
    async create(req, res) {
        // const {name} = req.body
        // const _reader = await reader.create({name})
        // return res.json(_reader)
    }
    async getAll(req, res) {
        const readers = await reader.findAll()
        return res.json(readers)
    }
}

module.exports = new ReaderController()