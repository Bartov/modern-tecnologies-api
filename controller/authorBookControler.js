const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const AuthorBook = models.author_book

const ApiError = require('../error/ApiError')

class AuthorController {
    async getAll(req, res) {
        const {id_author} = req.query
        let author_book;
        if(!id_author||id_author=='undefined'){
            author_book = await AuthorBook.findAll()
        }
        if (id_author&&id_author!='undefined') {
            author_book = await AuthorBook.findAll({where:{id_author}})
        }
        return res.json(author_book)
    }
    async get(req, res) { 
        try {
            const id_author = req.params.id
            const author_book = await AuthorBook.findOne(
                {where : {id_author}}
            )
            return res.json(author_book)
        }
        catch(e){
            return res.json(e.message)
        }
    }
}

module.exports = new AuthorController()