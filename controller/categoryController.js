const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const category = models.category

const ApiError = require('../error/ApiError')

class CategoryController {
    async create(req, res) {
        const {name} = req.body
        const _category = await category.create({name})
        return res.json(_category)
    }
    async getAll(req, res) {
        const categorys = await category.findAll()
        return res.json(categorys)
    }
}

module.exports = new CategoryController()