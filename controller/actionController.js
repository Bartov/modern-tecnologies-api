const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const Action = models.action

const ApiError = require('../error/ApiError')

class ActionController {
    async getAll(req, res) {
        const actions = await Action.findAll()
        return res.json(actions)
    }
}

module.exports = new ActionController()