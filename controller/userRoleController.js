const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const User_role = models.user_role

const ApiError = require('../error/ApiError')

class CategoryController {
    async getAll(req, res) {
        let user_role = await User_role.findAll()
        return res.json(user_role)
    }
    async update(req, res) {
        const {id_user} = req.body
        const user_role = await User_role.update(req.body, {where: {id_user: id_user}})
        return res.json(user_role)
    }
}

module.exports = new CategoryController()