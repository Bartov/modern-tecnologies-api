const ApiError = require('../error/ApiError')
const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const User = models.user_
const Role = models.role_
const User_role = models.user_role
const bcript = require('bcrypt')
const jwt = require('jsonwebtoken')

const generateJwt = (id, login, role) => {
    return jwt.sign(
        {id, login, role},
        process.env.JWT,
        {expiresIn: '24h'}
    )
}

class UserController {
    async getAll(req, res) {
        let users = await User.findAll()
        return res.json(users) // придумать как возвращать только имена 
    }
    async registration(req, res, next) {
        const {surname, name, patronymic, login, password} = req.body
        const role = 5; // по умолчанию пользователь
        if (!login || !password) {
            return next(ApiError.badRequest('Данные некоректны'))
        }
        const candidate = await User.findOne({where: {login}})
        if (candidate) {
            return next(ApiError.badRequest('Пользователь с таким логином существует'))
        }
        const hashPassword = await bcript.hash(password, 5)
        
        const user = await User.create({surname, name, patronymic, login, password: hashPassword})
        const user_role = await User_role.create({id_user: user.id_user, id_role: role, date_from:"1999-01-08", date_to:"1999-01-08"})
        
        const token = generateJwt(user.id_user, user.login, user_role.id_role)
        
        return res.json({token, userName: user.name, userSurname: user.surname, userRole: user_role.id_role})
    }
    async login(req, res, next) {
        const {login, password} = req.body
        const user = await User.findOne({where: {login}})
        const user_role = await User_role.findOne({where: {id_user: user.id_user}})
        if (!user) {
            return next(ApiError.badRequest('Пользователь не найден'))
        }
        const comparePassword = bcript.compareSync(password, user.password)
        if (!comparePassword) {
            return next(ApiError.badRequest('Неверный пароль'))
        }
        const token = generateJwt(user.id_user, user.login, user_role.id_role)
        return res.json({token, userName: user.name, userSurname: user.surname, userRole: user_role.id_role})
    }
    async check(req, res, next) {
        const user = await User.findOne({where: {login: req.login}})
        const user_role = await User_role.findOne({where: {id_user: user.id_user}})
        const token = generateJwt(user.id_user, user.login, user_role.id_role)
        res.json({message: "Четенько"})
    }
}

module.exports = new UserController()//