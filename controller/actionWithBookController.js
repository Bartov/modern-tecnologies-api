const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const ActionWithBooks = models.action_with_books
const ApiError = require('../error/ApiError')


class ActionWithBookController {
    async getAll(req, res) {
        let action_with_books = await ActionWithBooks.findAll()
        return res.json(action_with_books)
    }
    async create(req, res) {
        try {
            // const {id_action, id_reader, nam, date_form, date_to} = req.body
            const action_with_book = await ActionWithBooks.create(req.body)
            return res.json(action_with_book)
        }
        catch(e){
            return res.json(e.message)
        }
    }
    async delete(req, res) {
        const {nam} = req.params
        const action_with_book = await ActionWithBooks.destroy({where: {nam: nam}})
        return res.json(action_with_book)
    }

}

module.exports = new ActionWithBookController()