const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const role = models.role_

const ApiError = require('../error/ApiError')

class RoleController {
    async getAll(req, res) {
        let roles = await role.findAll()
        return res.json(roles)
    }
}

module.exports = new RoleController()