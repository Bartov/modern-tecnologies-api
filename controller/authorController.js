const sequelize = require('../db')
const initModels = require("../models/init-models").initModels;
const models = initModels(sequelize);
const Author = models.author

const ApiError = require('../error/ApiError')

class AuthorController {
    async getAll(req, res) {
        let authors;
        authors = await Author.findAll()
        return res.json(authors)
    }
}

module.exports = new AuthorController()