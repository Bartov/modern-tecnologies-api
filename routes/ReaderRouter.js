const Router = require('express')
const router =  new Router()
const readerController = require('../controller/readerController')
const {checkOneRole} = require('../middleware/checkMiddleware')

router.post('/', checkOneRole(1), readerController.create) // 1 соотвествует роли администратор
router.get('/', checkOneRole(1), readerController.getAll)


module.exports = router