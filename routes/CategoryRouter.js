const Router = require('express')
const router =  new Router()
const categoryController = require('../controller/categoryController')
const {checkOneRole} = require('../middleware/checkMiddleware')

router.post('/', checkOneRole(1), categoryController.create) // 1 соотвествует роли администратор
router.get('/', categoryController.getAll)


module.exports = router