const Router = require('express')
const router =  new Router()
const userRoleController = require('../controller/userRoleController')
const {checkOneRole} = require('../middleware/checkMiddleware')

// router.post('/', checkOneRole(1), userRoleController.create) // 1 соотвествует роли администратор
router.get('/', checkOneRole(1), userRoleController.getAll)
router.put('/:id', checkOneRole(1), userRoleController.update)

module.exports = router