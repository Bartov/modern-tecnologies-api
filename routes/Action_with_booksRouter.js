const Router = require('express')
const router =  new Router()
const actionWithBookController = require('../controller/actionWithBookController')
const {checkOneRole} = require('../middleware/checkMiddleware')

router.post('/',checkOneRole(1), actionWithBookController.create)
router.get('/',checkOneRole(1), actionWithBookController.getAll)
router.delete('/:nam',checkOneRole(1), actionWithBookController.delete)

module.exports = router