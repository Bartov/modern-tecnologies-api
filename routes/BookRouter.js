const Router = require('express')
const router =  new Router()
const bookController = require('../controller/bookController')
const {checkOneRole} = require('../middleware/checkMiddleware')

router.post('/', checkOneRole(1), bookController.create)
router.get('/', bookController.getAll)
router.get('/:nam',bookController.get)

module.exports = router