const Router = require('express')
const router =  new Router()
const userController = require('../controller/userController')
const authMiddleware = require('../middleware/authMiddleware.js')
const {checkOneRole} = require('../middleware/checkMiddleware')

router.post('/registration',userController.registration)
router.post('/login', userController.login)
router.get('/auth', authMiddleware ,userController.check)
router.get('/', checkOneRole(1), userController.getAll)

module.exports = router //