const Router = require('express')
const router =  new Router()
const roleController = require('../controller/roleController')

router.get('/',roleController.getAll)

module.exports = router