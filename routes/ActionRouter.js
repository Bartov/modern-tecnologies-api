const Router = require('express')
const router =  new Router()
const actionController = require('../controller/actionController')


router.get('/', actionController.getAll)


module.exports = router