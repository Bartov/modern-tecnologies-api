const Router = require('express')
const authorController = require('../controller/authorController')
const router =  new Router()

router.get('/', authorController.getAll)

module.exports = router