const Router = require('express')
const authorBookController = require('../controller/authorBookControler')
const router =  new Router()

router.get('/', authorBookController.getAll)
router.get('/:id', authorBookController.get)

module.exports = router